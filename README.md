# README

This README provides the application overview with the help of some screenshots and the required steps to get the application up and running.

## Overview

### Recipe list view
![Recipe list view](docs/recipe_list_view.png)

## Recipe list view
![Recipe list view](docs/recipe_detail_view.png)

## Overall flow
http://g.recordit.co/dCGH5S60dQ.gif

## Get it running
```bash
yarn install
yarn start
```

Default port in development mode is `3000`. You might want to change that since `recipes-backend` project will also be consuming the same port. Just in case `recipes-backend` project has a different port than `3000`, then you will need to adapt that in `.env` file for this project