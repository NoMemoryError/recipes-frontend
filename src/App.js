import React, { Component } from "react"
import "./App.css"

import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom"

import history from "appHistory"

import RecipeList from "containers/RecipeList"
import RecipeDetail from "containers/RecipeDetail"
import NoMatch from "components/NoMatch"

class App extends Component {
  state = {
    location: document.location,
  }

  componentDidMount() {
    this.unlisten = history.listen(this.onHistoryChange)
  }

  componentWillUnmount() {
    this.unlisten && this.unlisten()
  }

  onHistoryChange = (location) => {
    this.setState(() => ({ location }))
  }

  render() {
    const { location } = this.state
    return (
      <div className="App">
        <BrowserRouter>
          <Switch location={location}>
            <Route
              exact path="/" render={() => (
                <Redirect to="/recipes"/>
              )}/>
            <Route exact path="/recipes" component={RecipeList}/>
            <Route exact path="/recipes/:id" component={RecipeDetail}/>
            <Route component={NoMatch}/>
          </Switch>
        </BrowserRouter>
      </div>
    )
  }
}

export default App
