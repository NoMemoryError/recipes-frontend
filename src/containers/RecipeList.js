import React, { Component, Fragment } from "react"
import PropTypes from "prop-types"

import api from "api"
import history from "appHistory"

import { ListGroup, Spinner } from "reactstrap"
import RecipeItem from "components/RecipeItem"
import NoItems from "components/NoItems"

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL
class RecipeList extends Component {
  static propTypes = {
    location: PropTypes.object,
  }

  state = {
    recipes: [],
    loading: true,
  }

  componentDidMount() {
    this.queryRecipes()
  }

  queryRecipes = () => {
    api({
      href: `${API_BASE_URL}/recipes`,
      method: "GET",
    }).then(({ data }) => {
      this.setState({ recipes: data, loading: false })
    })
  }

  onItemClick = (id) => {
    const { location } = this.props
    history.push(`${location.pathname}/${id}`)
  }
  
  render() {
    const { recipes, loading } = this.state
    if (loading) {
      return <div className="text-center"><Spinner color="primary"/></div>
    }
    if (recipes && recipes.length === 0) {
      return <NoItems/>
    }

    return <Fragment>
      <h3>Recipes List</h3>
      <ListGroup>
        {
          recipes.map(({ id, title, description, tags }) => (
            <RecipeItem
              key={id}
              id={id}
              title={title}
              description={description}
              tags={tags}
              onClick={this.onItemClick}/>
          ))
        }
      </ListGroup>
    </Fragment>
  }
}

export default RecipeList
