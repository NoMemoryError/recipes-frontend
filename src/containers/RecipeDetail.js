import React, { Component } from "react"
import PropTypes from "prop-types"

import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Spinner,
} from "reactstrap"

import api from "api"

import RecipeTags from "components/RecipeTags"
import NotFound from "components/NotFound"

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL
class RecipeDetail extends Component {
  static propTypes = {
    match: PropTypes.object,
  }

  state = {
    recipe: {},
    error: false,
    loading: true,
  }

  componentDidMount() {
    this.queryRecipeDetail()
  }

  queryRecipeDetail = () => {
    const { params: { id } } = this.props.match
    api({
      href: `${API_BASE_URL}/recipes/${id}`,
      method: "GET",
    }).then(({ data }) => {
      this.setState({ recipe: data, loading: false })
    }).catch(() => {
      this.setState({ error: true, loading: false })
    })
  }

  render() {
    const { recipe, error, loading } = this.state
    const { title, tags, description, image_url, chef_name } = recipe
    if (error) {
      return <NotFound/>
    }

    return loading ?
      <div className="text-center"><Spinner color="primary"/></div> :
      <Card>
        <CardImg top width="100%" src={image_url} alt="Recipe image"/>
        <CardBody>
          <CardTitle>{title}</CardTitle>
          <CardSubtitle>{chef_name}</CardSubtitle>
          <CardText>{description}</CardText>
          <RecipeTags tags={tags}/>
        </CardBody>
      </Card>
  }
}

export default RecipeDetail
