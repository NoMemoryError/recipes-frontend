import React from "react"

const NotFound = () => (
  <h3>
    Recipe not found. You might have a typo.
    <span role="img" aria-label="winking face"> 🤪😉 </span>
  </h3>
)

export default NotFound
