import React from "react"

const NoMatch = () => (
  <h3>
    404. That’s an error.
    <span role="img" aria-label="goofy face"> 🤪 </span>
  </h3>
)

export default NoMatch
