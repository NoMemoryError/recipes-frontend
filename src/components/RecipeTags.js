import React from "react"
import PropTypes from "prop-types"

import { Badge } from "reactstrap"

const PROPTYPES = {
  tags: PropTypes.array,
}

const RecipeTags = ({ tags }) => (
  <div>
    {
      tags && tags.map((tag, index) =>
        <Badge key={index} color="info" pill>{tag}</Badge>
      )
    }
  </div>
)

RecipeTags.propTypes = PROPTYPES
export default RecipeTags
