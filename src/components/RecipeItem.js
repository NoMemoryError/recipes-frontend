import React from "react"
import PropTypes from "prop-types"

import { ListGroupItem, ListGroupItemHeading, ListGroupItemText } from "reactstrap"

import RecipeTags from "components/RecipeTags"

const PROPTYPES = {
  id: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  tags: PropTypes.array,
  onClick: PropTypes.func,
}

const RecipeItem = ({ id, title, description, tags, onClick }) => (
  <ListGroupItem>
    <ListGroupItemHeading className="btn-link" onClick={() => { onClick(id) }}>
      { title }
    </ListGroupItemHeading>
    <ListGroupItemText>
      { description }
    </ListGroupItemText>
    <RecipeTags tags={tags}/>
  </ListGroupItem>
)

RecipeItem.propTypes = PROPTYPES
export default RecipeItem
