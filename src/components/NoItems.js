import React from "react"

const NoItems = () => (
  <h3>
    Unfortunately there are no recipes available
    <span role="img" aria-label="goofy face"> 🤪 </span>
  </h3>
)

export default NoItems
